var config = {
	map: {
		'*':{
			'owlcarousel': "js/owl.carousel.min",
			'jquery/slick': 'js/slick.min',
			'jquery/matchHeight':'js/jquery.matchHeight-min'
		}
	},
	shim: {
		'jquery/slick': {
			deps: ['jquery']
		},
		'jquery/matchHeight': {
			deps: ['jquery']
		},
		'owlcarousel': {
			deps: ['jquery']
		}
	}
};
