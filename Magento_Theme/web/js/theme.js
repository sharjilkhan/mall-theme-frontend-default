/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'matchMedia',
    'jquery/matchHeight',
    'domReady!'
], function ($, keyboardHandler, matchMedia) {
    'use strict';

    if ($('body').hasClass('checkout-cart-index')) {
        if ($('#co-shipping-method-form .fieldset.rates').length > 0 &&
            $('#co-shipping-method-form .fieldset.rates :checked').length === 0
        ) {
            $('#block-shipping').on('collapsiblecreate', function () {
                $('#block-shipping').collapsible('forceActivate');
            });
        }
    }

    $('.cart-summary').mage('sticky', {
        container: '#maincontent'
    });

    $('.panel.header > .header.links').clone().appendTo('#store\\.links');

    keyboardHandler.apply();


    mediaCheck({
        media: '(max-width: 767px)',
        // Switch to Desktop Version
        entry: function () {
            $('.top-links-custom').appendTo($('.section-item-content.nav-sections-item-content[id="store.links"]'));
            $('.panel.header .authorization-link').appendTo($('.section-item-content.nav-sections-item-content[id="store.links"]'));
        },
        // Switch to Mobile Version
        exit: function () {
            $('.section-item-content.nav-sections-item-content[id="store.links"] .top-links-custom').insertBefore($('.block.block-search'));
            $('.section-item-content.nav-sections-item-content[id="store.links"] .authorization-link').appendTo('.panel.header');
        }
    });

    $('.products-grid .product-item-name').matchHeight();
    $('.products-grid .price-box.price-final_price').matchHeight();
});
